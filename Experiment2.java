package Assignment4;
import java.util.*;


/**
 * Experiment2 takes the input array and searches for each entry in an Linked List of random integers.
 * Then, it reports the number of found inputs and length of time it took to find them all.
 * @author Zachary Subealdea
 */
public class Experiment2
{

	public static void experiment ( ArrayList<Integer> inputArray,ArrayList<Integer> randArray)
	{
		LinkedList<Integer> randomList = new LinkedList<Integer>();
		randomList.addAll(randArray);
		
		StopWatch timer = new StopWatch();
		int values_found = 0;
		int values_notfound = 0;
		int inputs_size = inputArray.size(); 
		
		System.out.println("Experiment 2:");
		timer.start();
		for(int i=0; i< inputs_size; i++)
		{	
			int value= inputArray.get(i);
			int index = randomList.indexOf(value);
			if (index >= 0)
			{	timer.stop();
				values_found++;
				timer.start();
			}
			else
			{	timer.stop();
				values_notfound++;
				timer.start();
			}
		}
		timer.stop();
		System.out.println("Number of found values: " + values_found);
		System.out.println("Number of values not found: " + values_notfound);
		System.out.println("Time elapsed in seconds is: "+ timer.getElapsedTime() / StopWatch.NANOS_PER_SEC + "\n\n");
	}
}
