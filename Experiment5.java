package Assignment4;

import java.util.ArrayList;
/** 
 * Takes the input array and performs an Interpolation search for each input in the sorted array of random integers.
 * Then, it reports the number of found inputs and length of time it took to find them all.
 * Experiment5 can be run in verbose mode by prompting the user.
 * Verbose mode reports which values it finds if there are less than 100 found.
 * similiar implementatation is found at link http://en.wikipedia.org/wiki/Interpolation_search 
 * The link is found in the lab manual for reference
 * @author Zachary Subealdea
 */
public class Experiment5 
{
	public static void experiment5 ( ArrayList<Integer> inputArray,ArrayList<Integer> sorted_randArray)
	{
		ArrayList<Integer>foundArray=new ArrayList<Integer>(); 
		ArrayList<Integer>notfoundArray=new ArrayList<Integer>();
	
		
		System.out.println("Experiment 5:");
		StopWatch timer = new StopWatch();
		int values_found = 0;
		int values_notfound = 0;
		int inputs_size = inputArray.size(); 
			
		boolean verbosemode= assignment4Driver.Verboseprompt(); 

		timer.start();
 		for(int i=0; i< inputs_size; i++)
		{	
 			boolean valueLocated = false; 
			int value= inputArray.get(i);
			int index = 0,mid=0, low=0, high= sorted_randArray.size()-1;
			//
			while( (sorted_randArray.get(low) <= value) && (value <= sorted_randArray.get(high) ) )
			{
				double a= value - (double)sorted_randArray.get(low),
					   b= (double)sorted_randArray.get(high) - (double)sorted_randArray.get(low),
					   c= (high - low);
				
				double d= a*c /b;
				mid = low + (int)Math.ceil( d);
				
				if (sorted_randArray.get(mid) < value)  
				{	low = mid + 1;
				}  
				else if( sorted_randArray.get(mid) > value )
				{ 	high = mid - 1; 
				}
				else if (value==sorted_randArray.get(mid))
				{	index = mid;
					valueLocated=true; break;
				}
			}
			if (valueLocated ==false)
			{
				if(value == sorted_randArray.get(low)) {index = low; }
				else index = -1; 
			}
			if (index >= 0)
			{	timer.stop();
				values_found++;
				foundArray.add(sorted_randArray.get(index));
				timer.start();
			}
			else
			{	timer.stop();
				values_notfound++;
				notfoundArray.add(value);
				timer.start();
			}
		}
		timer.stop();
		System.out.println("Number of found values: " + values_found);
		if ((verbosemode==true) && (values_found<100))
		{System.out.println(""+ foundArray.toString() );}
		System.out.println("Number of values not found: " + values_notfound);
		if ((verbosemode==true) && (values_notfound<100))
		{System.out.println(""+ notfoundArray.toString() );}
		System.out.println("Time elapsed in seconds is: "+ timer.getElapsedTime() / StopWatch.NANOS_PER_SEC + "\n\n");
	}
}