package Assignment4;

import java.util.*;

/**
 * Combined Experiement 3 and 4:
 * It sorts the array of random integers and searches for input values within it
 * Then, reports the number of found inputs and length of time it took to find them all
 * @author Zachary Subealdea
 */
public class Experiments_3_and_4
{

	/*------experiment3------
	 * 
	 * It takes the array of random numbers and sorts it
	 * Then, it reports the time it takes to sort
	 * postcondition: the array of random integers is sorted for the rest of the experiments
	 * 
	 */
	public static void experiment3 ( ArrayList<Integer> inputArray,ArrayList<Integer> randArray)
	{
		
		StopWatch timer = new StopWatch();
		
		System.out.println("Experiment 3:");
		timer.start();
		Collections.sort(randArray);
		timer.stop();
		System.out.println("Time elapsed for sorting in seconds is: "+ timer.getElapsedTime() / StopWatch.NANOS_PER_SEC + "\n\n");
	}
	
	/*--------experiment4---------
	 * It takes the sorted array of random numbers performs binary searches for each input value.
	 * Then, it reports the time it takes to find each value
	 * 
	 */
	public static void experiment4 ( ArrayList<Integer> inputArray,ArrayList<Integer> sorted_randArray)
	{
	
		StopWatch timer = new StopWatch();
		int values_found = 0;
		int values_notfound = 0;
		int inputs_size = inputArray.size(); 
			
		System.out.println("Experiment 4:");
		timer.start();
		for(int i=0; i< inputs_size; i++)
		{	
			int value= inputArray.get(i);
			int index = Collections.binarySearch(sorted_randArray,value);
			if (index >= 0)
			{	timer.stop();
				values_found++;
				timer.start();
			}
			else
			{	timer.stop();
				values_notfound++;
				timer.start();
			}
		}
		timer.stop();
		System.out.println("Number of found values: " + values_found);
		System.out.println("Number of values not found: " + values_notfound);
		System.out.println("Time elapsed in seconds is: "+ timer.getElapsedTime() / StopWatch.NANOS_PER_SEC + "\n\n");
	}
}
