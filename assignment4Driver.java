package Assignment4;
import java.util.*;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


/**
 * This program takes an input file and searches for each of its integer inputs in a list of 1000000 random integers.
 * Then, it reports the number of found inputs and length of time it took to find them all.
 * precondition: a text file exists in the directory. The file should have a list of integers.
 * @author Zachary Subealdea
 */

public class assignment4Driver 
{

	private static ArrayList<Integer> input_vals = new ArrayList<Integer>();
	private static ArrayList<Integer> randArray = new ArrayList<Integer>() ;
	
	public static void main(String[] args) 
	{

		get_inputvalues (args); 
		//WriteFile();
		RandomMillion(1000000); 
		Experiment1.experiment(input_vals, randArray);
		Experiment2.experiment(input_vals, randArray);
		Experiments_3_and_4.experiment3(input_vals, randArray);
		Experiments_3_and_4.experiment4(input_vals, randArray);
		Experiment5.experiment5(input_vals, randArray); 
		
		
	}

	
	
	
	
	/*----------- get_inputvalues ------------------/
	*	retrieves data from a file: input.txt
	*	creates an array of integers and an invalid input array 
	*/
	
	private static void get_inputvalues (String[] args)
	{
		ArrayList<String> illegal_vals = new ArrayList<String>();
		
		if (args.length < 1) 
		{
			System.out.println("Error: send name of file");
		}
		try (Scanner inputfile = new Scanner( new FileReader(args[0]));)
		{
			while( inputfile.hasNextLine( ) )
			{	String textline = inputfile.nextLine( );
				try
				{
					Integer number = Integer.parseInt(textline);
					if (number<0){throw new NumberFormatException();}
					input_vals.add(number);
				}
				catch(NumberFormatException e)
				{	illegal_vals.add(textline);
				}
				
			}
		}
		catch( IOException e ) { System.out.println( e ); }
		System.out.println("Number of illegal values in input file: " + illegal_vals.size());
		if (illegal_vals.size() < 30){System.out.println(""+ illegal_vals.toString() );}
		System.out.println("\n\n");
	}

	/*------------- RandomMillion ------------------------/
	 * Creates the array of random integers
	 * Input: The number of random integers in the array
	 */
	private static void RandomMillion (int size)
	{
		Random rand = new Random(15485863);//853
		for (int i = 0; i < size; i++)
		{
			int number= rand.nextInt(Integer.MAX_VALUE) +1;
			randArray.add(number);
		}
		/* values used for debug
		randArray.add(2026431669); 
		randArray.add(905089477);
		randArray.add(1110101096);
		randArray.add(1894165735);
		randArray.add(25395550);
		*/
	}

	
	
	/*-----------------------Utilities----------------------------*/
	
	/**----------- WriteFile ------------------/
	 * 
	 *  Used in debug
	 *  Creates a Text file named input.txt of 1000 random integers
	 *
	 */
	private static void WriteFile ()
	{
	    //FileWriter fWriter = null;
	    //BufferedWriter writer = null;
	    try 
	    {
	      FileWriter fWriter = new FileWriter("input.txt");
	      BufferedWriter writer = new BufferedWriter(fWriter);
	      Random rando = new Random(15485863);
	      for (int i = 0; i < 1000; i++)
	      {
	    	  
	    	  int number= rando.nextInt(Integer.MAX_VALUE) +1;
	    	  String s= ""+number;
	    	  writer.write(s);
	    	  writer.newLine();
	      }
	     
	      writer.close();
	    } 
	    catch (Exception e)
	    {	System.out.println("Error!");
	    }
	}

	/**-----Verboseprompt-------/
	 * 
	 * Prompts the user to run the experiment in verbose mode
	 * returns the users answer
	 */
	public static boolean Verboseprompt()
	{
		Scanner input = new Scanner(System.in);
		while (true) 
		{
			System.out.print("Run in Verbose? (Y/N): ");
			String answer = input.nextLine();
			
			if (answer.equals("Y") || answer.equals("y")) 
			{ 	input.close();
				return true;
			}
			else if (answer.equals("N") || answer.equals("n")) 
			{ 	input.close();
				return false; 
			}
			else
			{
				System.out.println("Invalid response.\n");
			}
		}
	}
}